import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Button, Row, Col, Card } from 'react-bootstrap';

export default function UserTransactionsCard({ userTransaction }) {
  const { user } = useContext(UserContext);

  const {
    userId,
    totalAmount,
    subtotal,
    shippingFee,
    voucherDiscount,
    status,
    books,
    paymentMethod,
    _id,
    createdOn
  } = userTransaction;

  return (
    <>
      <Row className="mt-3 mb-3">
        <Col xs={12}>
          <Card className="cardHighlight p-0">
            <Card.Body>
              <Card.Subtitle>Order ID</Card.Subtitle>
              <Card.Text>{_id}</Card.Text>
              <Card.Subtitle>Date ordered</Card.Subtitle>
              <Card.Text>{createdOn}</Card.Text>
              <Card.Subtitle>Status</Card.Subtitle>
              <Card.Text>{status}</Card.Text>
              <Card.Subtitle>Total Amount</Card.Subtitle>
              <Card.Text>{totalAmount}</Card.Text>
              <Card.Subtitle>Subtotal</Card.Subtitle>
              <Card.Text>{subtotal}</Card.Text>
              <Card.Subtitle>Shipping Fee</Card.Subtitle>
              <Card.Text>{shippingFee}</Card.Text>
              <Card.Subtitle>Voucher Discount</Card.Subtitle>
              <Card.Text>{voucherDiscount}</Card.Text>
              <Card.Subtitle>Books</Card.Subtitle>
              <Card.Text>
                {books.map((book) => (
                  <div key={book.id}>
                    <Link to={`/books/${book.bookId}`}>{book.title}</Link>
                    <br />
                    Author: {book.author}
                    <br />
                    Price: {book.price}
                    <br />
                    Quantity: {book.quantity}
                    <br />
                  </div>
                ))}
              </Card.Text>
              <Card.Subtitle>Payment Method</Card.Subtitle>
              <Card.Text>{paymentMethod}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
}
