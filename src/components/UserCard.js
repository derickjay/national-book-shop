import {useState,useEffect} from 'react';
import {Link} from "react-router-dom"
// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function UserCard({user}) {

    const {name,username,location,_id,email,password,isAdmin,wishlist} = user;


return (
 <Row className="mt-3 mb-3">
  <Col xs={12}>
   <Card className="cardHighlight p-0">
    <Card.Body>
     <Card.Title><h4>{name}</h4></Card.Title>
     <Card.Subtitle>username</Card.Subtitle>
     <Card.Text>{username}</Card.Text>
     <Card.Subtitle>email</Card.Subtitle>
     <Card.Text>{email}</Card.Text>
     <Button className="bg-primary" as={Link} to={`/users/${_id}/view`}>More Details </Button>
    </Card.Body>
   </Card>
  </Col>
 </Row>        
    )
}