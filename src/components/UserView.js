import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


import {useParams, useNavigate, Link} from 'react-router-dom'


export default function UserView() {

	const{user} = useContext(UserContext)

	const navigate = useNavigate('')

	const {userId} = useParams()
	
	const [name, setName] = useState("");
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [location, setLocation] = useState({
	  completeAddress: "",
	  barangay: "",
	  municipality: "",
	  province: "",
	  region: ""
	});


	const enroll = (userId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsAdmin`, {
			method: "PATCH",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have successfully added the user as admin."
				})

				navigate("/users/all")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/view`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);


				setName(data.name);
				setUsername(data.username);
				setEmail(data.email);
				setLocation(data.location);
			})
	}, [userId])

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Email:</Card.Subtitle>
					        <Card.Text>{email}</Card.Text>
					        <Card.Subtitle>Username:</Card.Subtitle>
					        <Card.Text>{username}</Card.Text>
					        <Card.Subtitle>Location:</Card.Subtitle>
					        <Card.Text>{location.completeAddress}, {location.barangay}, {location.municipality}, {location.province}, {location.region}</Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" onClick={() => enroll(userId)} >Set as Admin</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Enroll</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
