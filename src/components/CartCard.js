import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CartCard({ order }) {
  const { books, subtotal, _id } = order;
  const { user } = useContext(UserContext);
  const [quantity, setQuantity] = useState(books.quantity);
  const [newSubtotal, setNewSubtotal] = useState(subtotal);

  const handleIncrease = () => {
    setQuantity(prevQuantity => prevQuantity + 1);
  };

  const handleDecrease = () => {
    setQuantity(prevQuantity => prevQuantity - 1);
  };

  const handleDelete = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/${order._id}/delete`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          // Remove the card if API call is successful
          document.getElementById(`card${_id}`).remove();
        }
      })
      .catch(error => console.log(error));
  };

 


  useEffect(() => {
    // Make API call to update quantity and subtotal when quantity changes
    const data = { quantity: quantity };
    fetch(`${process.env.REACT_APP_API_URL}/orders/${order._id}/changeQuantity`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(data)
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          // Update the subtotal if API call is successful
          setNewSubtotal(quantity * books.price);
        } else if(data.requestExceedsStock) {
          Swal.fire({
            title: 'Requested quantity exceeds stock',
            icon: 'warning',
            text: 'Please check out',
          });
          setQuantity(books.stock)
        }
      })
      .catch(error => console.log(error));
  }, [quantity, order._id, books.price]);

  return (
  <Card id={`card${_id}`} className="cardHighlight p-0 mb-3">
    <Card.Body className="d-flex justify-content-md-start justify-content-sm-between align-items-center">
      <div>
        <Card.Title>
          <Button variant="danger" onClick={handleDelete}>x</Button>
        </Card.Title>
      </div>
      <div className="ps-5">
        <Card.Text><b>Title:</b> {books.title}</Card.Text>
      </div>
      <div  className="pe-5 ps-5">
        <Card.Text><b>Price:</b> {books.price}</Card.Text>
      </div>
      <div >
        <Card.Text>
          <b>Quantity:</b>
          </Card.Text>
      </div>
          <div className="pe-5 ps-5" style={{ display: 'flex', alignItems: 'center' }}>
            <Button variant="outline-secondary" onClick={handleDecrease} disabled={quantity === 1}>-</Button>
            <input
              type="number"
              value={quantity}
              min="0"
              onChange={event => {
                const input = event.target.value;
                // Check if input is a valid number and not 0
                if (!isNaN(input) && input !== '' && input !== '0') {
                  setQuantity(parseInt(input));
                }
              }}
              style={{ width: '40px', textAlign: 'center', margin: '0 5px' }}
            />
            <Button variant="outline-secondary" onClick={handleIncrease} disabled={quantity === books.stock}>+</Button>
            {quantity === books.stock ? <span style={{ color: 'red' }}> (no more stock)</span> : ''}
          </div>
      <div className="pe-3">
        <Card.Subtitle>Subtotal:</Card.Subtitle>
      </div>
      <div>
        <Card.Text>{newSubtotal}</Card.Text>
      </div>
    </Card.Body>
  </Card>
);


}
