import {useState, useEffect} from 'react';
import BookArchivesCard from "../components/BookArchivesCard";
import { Row, Col, Container, Dropdown, DropdownButton , Button} from 'react-bootstrap';

export default function BookArchives() {

    const [books, setBooks] = useState([]);
    const [refresh, setRefresh] = useState(false);


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/books/archives`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setBooks(data);
        })
    }, [])


      useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/books/archives`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setBooks(data);
        })
    }, [refresh])


   const handleRefresh = () => {

   	setRefresh(!refresh)
   }


  const [sortOption, setSortOption] = useState(null);
  const [filterGenre, setFilterGenre] = useState(null);

  const sortedBooks = [...books]
    .filter((book) => (filterGenre ? book.genre === filterGenre : true))
    .sort((a, b) => {
      if (!sortOption) return 0;
      if (sortOption === 'highest-price') return b.price - a.price;
      if (sortOption === 'lowest-price') return a.price - b.price;
      if (sortOption === 'highest-sold') return b.sold - a.sold;
      if (sortOption === 'lowest-sold') return a.sold - b.sold;
      if (sortOption === 'highest-star') return b.rating - a.rating;
      if (sortOption === 'lowest-star') return a.rating - b.rating;
      if (sortOption === 'highest-rating') return b.ratingCount - a.ratingCount;
      if (sortOption === 'lowest-rating') return a.ratingCount - b.ratingCount;
      if (sortOption === 'highest-stock') return b.stock - a.stock;
      if (sortOption === 'lowest-stock') return a.stock - b.stock;
      return 0;
    });

  const genres = ['All Genres', 'Fantasy', 'Self-help', 'Romance'];


   return (
    <Container>
      <Row>
      	<h1 className="text-center border my-3 w-100 bg-danger text-warning w-50">&#11015;Books for sale&#11015;</h1>
        <div className="d-flex align-items-center mt-3 justify-content-start">
        <div className="me-5 ">
          <DropdownButton className="_sort_button" title="Sort By" className="mr-2">
            <Dropdown.Item onClick={() => setSortOption('highest-price')}>
              Highest Price
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-price')}>
              Lowest Price
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-sold')}>
              Highest Sold
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-sold')}>
              Lowest Sold
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-star')}>
              Highest Star
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-star')}>
              Lowest Star
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-rating')}>
              Highest Rating
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-rating')}>
              Lowest Rating
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('highest-stock')}>
              Highest Stock
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setSortOption('lowest-stock')}>
              Lowest Stock
            </Dropdown.Item>
          </DropdownButton>
         </div>
          {genres.map((genre) => (
            <Button
              key={genre}
              onClick={() => setFilterGenre(genre === 'All Genres' ? null : genre)}
              variant={filterGenre === genre ? 'danger' : 'secondary'}
              className="_genre_buttons">{genre}
			</Button>
				))}
		</div>
		</Row>
		<Row>
			{sortedBooks.map((book) => (
		<Col sm={6} md={4} lg={3} key={book.id} className="book-card-col">
		<div className="card-container">
		<BookArchivesCard book={book} handleRefresh={handleRefresh}/>
		</div>
		</Col>
		))}
		</Row>
		</Container>
		);
}