import {Card,Row,Col,Button} from "react-bootstrap"
import {useState, useEffect,useContext} from 'react';
import Swal from 'sweetalert2';
import {Link} from "react-router-dom"


export default function UserDetails() {


	const [userDetails, setUserDetails] = useState({
  name: '',
  username: '',
  email: '',
  booksPurchased: [],
  location: {
    region: '',
    province: '',
    municipality: '',
    barangay: '',
    completeAddress: ''
  },
  wishlist: [],
  shippingAddresses: []
});
	const {name,username,email,booksPurchased,location,wishlist,shippingAddresses} = userDetails;
    const {region, province, municipality, barangay, completeAddress} = location




useEffect(() => {
    // Make API call to update quantity and subtotal when quantity changes
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
        	console.log(data)
          // Update the subtotal if API call is successful
          setUserDetails(data)
        } else {
        	console.log(data)
          Swal.fire({
            title: 'Error',
            icon: 'warning',
            text: 'Please check out',
          });
        }
      })

  }, []);

	


return (
	
	<Row className="mt-3">
          <Col>
              <Card className="cardHighlight p-3">
                  <Card.Body>
                      <Card.Title>
                          <h2>Your Details</h2>
                      </Card.Title>
                      <Card.Text>Name: {name}</Card.Text>
                      <Card.Text>Username: {username}</Card.Text>
                      <Card.Text>Email: {email}</Card.Text>
                      <Card.Text>Password: <Button as={Link} to="/changePassword"variant="secondary">Change Password</Button></Card.Text>
                      <Card.Text>Address: {completeAddress},{barangay},{municipality},{province},{region}</Card.Text>
                      <Card.Text>Books Purchased: {booksPurchased && booksPurchased.join(',')}</Card.Text>
                      {/*<Card.Text>Wishlists: {wishlist && wishlist.join(',')}</Card.Text>*/}
                      <Button variant="info">View your shippingAddresses</Button>
                  </Card.Body>
              </Card>
          </Col>
      </Row>
	)

}