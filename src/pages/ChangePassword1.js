import { useState, useEffect } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Button, Form, Container, Row, Col, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ChangePassword() {
  const navigate = useNavigate('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [oldPassword, setOldPassword] = useState('');
  const [showIncorrectOldPassword, setShowIncorrectOldPassword] = useState(false);
  const [passwordsMatch, setPasswordsMatch] = useState(true);
  const [oldPasswordsMatch, setOldPasswordsMatch] = useState(true);

  function changePassword(e) {
  	e.preventDefault()
    fetch(`${process.env.REACT_APP_API_URL}/users/changePassword`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        password: password1,
        confirmPassword: password2,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: 'Password changed!',
            icon: 'success',
          });
          navigate('/users/details');
        } else if (data.passwordMatches) {
          Swal.fire({
            title: 'Password same as old one!',
            icon: 'warning',
          });
        } else if (!data.passwordMatches) {
          Swal.fire({
            title: `${data.message}!`,
            icon: 'warning',
          });
        } else {
          console.log(data);
        }
      });
  }

  useEffect(() => {
    if (oldPassword) {
      fetch(`${process.env.REACT_APP_API_URL}/users/checkOldPassword`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({
          password: oldPassword,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setShowIncorrectOldPassword(!data);
        });
    }
  }, [oldPassword]);

  const handleConfirmPassword = () => {
    setPasswordsMatch(password1 === password2);
  };


  const handleNewPassword = () => {
    setOldPasswordsMatch(password1 === oldPassword);
  };


  return (
    <Container>
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Header>Change Password</Card.Header>
            <Card.Body>
              <Form onSubmit={changePassword}>
                <Form.Group controlId="oldPassword">
                  <Form.Label>Old Password</Form.Label>
                  <Form.Control
                    type="password"
                    value={oldPassword}
                    onChange={(e) => setOldPassword(e.target.value)}
                    isInvalid={showIncorrectOldPassword}
                  />
                  <Form.Control.Feedback type="invalid">
                    Incorrect old password!
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="newPassword">
                  <Form.Label>New Password</Form.Label>
                  <Form.Control
                    type="password"
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    isInvalid={ password1 === oldPassword && password1 !== '' }
                    />
                    <Form.Control.Feedback type="invalid">
                    New password is same as old password!
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="confirmNewPassword">
                  <Form.Label>Confirm New Password</Form.Label>
                  <Form.Control
                    type="password"
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    isInvalid={password2 !== '' && password2 !== password1}
                    />
                    <Form.Control.Feedback type="invalid">
                    Passwords do not match!
                    </Form.Control.Feedback>
                </Form.Group>
                <Button
                  variant="primary"
                  type="submit"
                  disabled={
                    !oldPassword ||
                    !password1 ||
                    !password2 ||
                    password1 !== password2 ||
                    showIncorrectOldPassword
                  }
                >
                  Submit
                </Button>
							<Button 
								variant="secondary" 
								as={Link} 
								to="/users/details" 
								className="ml-2"
							>
								Cancel
							</Button>
						</Form>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>
)









}